const CopyWebpackPlugin = require("copy-webpack-plugin");
const webpack = require("webpack");

module.exports = {
  transpileDependencies: ["vuetify"],

  publicPath: process.env.NODE_ENV === "production" ? "./" : "./",

  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      var c = require("./src/assets/config.json");
      args[0].title = c.appName;
      return args;
    });
  },

  configureWebpack: {
    devtool: "source-map",
    resolve: {
      fallback: {
        "path": require.resolve("path-browserify"),
        "crypto": false, //require.resolve("crypto-browserify"),
        "stream": require.resolve("stream-browserify"),
        "fs": require.resolve("browserify-fs"),
        "buffer": require.resolve("buffer")
      }
    },
    plugins: [
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
      }),
      new webpack.ProvidePlugin({
        process: 'process/browser',
    }),
      new CopyWebpackPlugin({patterns: [
        {
          from: "./src/assets/config.json",
          to: "./",
        },
        {
          from: "./node_modules/@matrix-org/olm/olm.wasm",
          to: "./js/olm.wasm",
        },
      ]}),
    ],
  },

  devServer: {
    //https: true,
  },
};
