# Convene Directly

A version of [Convene](https://gitlab.com/keanuapp/keanuapp-weblite) that allows people to contact you directly with no account or link needed to be shared. Your specified accounts are automatically invited upon room creation and the preferred power levels are set.

## Features

* Standalone web client with a responsive, mobile-web tuned user interface
* Built upon the Matrix protcol, with full support for end-to-end encrypted messaging, and completely interoperable with any other Matrix client
* Progressive Web App capabilities
* Full multimedia upload and download: images, audio, video
* Built-in push-to-record voice messaging
* Quick room switcher
* Ability to create new rooms with name, topic and avatar icon
* Invite people to room using QR code or room invite link
* Quick replies or full reply to any message
* Message editing and deletion based on "Power Levels" (Moderator, Admin, etc) 

## Configure Directly

**⚠️ READ THIS BEFORE RUNNING!**

Go to [`src/components/CreateRoom.vue`](src/components/CreateRoom.vue#L559) and find the `invitees` object. Enter the account ID:s and power levels you want.

_This will be moved into a configuration file. But in the meantime, it is hardcoded._

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Theming

# Sticker short codes - To enable sticker short codes, follow these steps:
* Run the "create sticker config" script using "npm run create-sticker-config <path-to-sticker-packs>"
* Insert the resulting config blob into the "shortCodeStickers" value of the config file (assets/config.json)
* Rearrange order of sticker packs by editing the config blob above.
